﻿using System;
using System.Collections.Generic;
using System.Collections;

namespace Neoception_Example_App
{
    class Program
    {

        public string name = "Diogo";
        static void Main(string[] args)
        {
            int num = 6;
            for (int i = 0; i < num; i++)
            {
                Console.WriteLine(i);
            }

            //printname();
            //ManageNames.printBookAndAge();
            //exceptionHandling();
            //handlingArrays.insertArray();
            //handlingArrays.regularArrays();
            //handlingArrays.nDimensional();
            handlingArrays.cSharpDicts();
        }

        static void printname()
        {
            Program new_name = new Program();
            Console.WriteLine(new_name.name);
        }


        static void exceptionHandling()
        {
            try
            {
                Console.Write("Enter a number: ");
                int num1 = Convert.ToInt32(Console.ReadLine());
                Console.Write("Enter a number: ");
                int num2 = Convert.ToInt32(Console.ReadLine());

                int division = num1 / num2;
                Console.WriteLine("Result:\n" + division);
            }

            catch (Exception e)
            {
                //Prints out the exception message
                Console.WriteLine(e.Message);
            }

            Console.ReadLine();
        }
    }

    public class ManageNames
    {
        public string bookName = "Harry Potter";
        public int age = 10;

        public static void printBookAndAge()
        {
            //Create new object for class ManageNames
            ManageNames book = new ManageNames();
            string name = book.bookName;

            if (name != "")
            {
                Console.WriteLine("Here's the name: " + name);
            }

        }
    }

    public class handlingArrays
    {
        /*
         * ***************************************************************************************
         * Array Lists are more flexible arrays 
         * They represent an ordered collection of an object that can be indexed individually 
         * It is basically an alternative to an array
         * ****************************************************************************************
         */
        public static void insertArray()
        {
            //Create a new array list
            ArrayList myArray = new ArrayList();

            myArray.Add("Diogo");

            //Create a ranged array list
            ArrayList myArray2 = new ArrayList();
            myArray2.AddRange(new object[]
            {
                "Diogo",
                "Alberto",
                "Maria"
            });

            //Sort the array list
            myArray2.Sort();
            myArray2.Add("Ana");

            //Loop through the ArrayList
            foreach (var item in myArray2)
            {
                Console.WriteLine("Here's the array list\n" + item);
            }
        }

        public static void regularArrays()
        {
            string[] cars = { "Volvo", "Volkswagen", "Opel" };

            for (int i = 0; i < cars.Length; i++)
            {
                Console.WriteLine(cars[i]);
            }
        }

        public static void nDimensional()
        {
            //A N-dimensional array is a collection of N array objects of the same data type
            int[][] matrix =
            {
                new int[] {1,2,3},
                new int[] {4,5,6}
            };

            matrix[0][1] = 10;
            Console.WriteLine(matrix[0][1]);
        }

        public static void cSharpDicts()
        {
            Dictionary<int, string> studentNumber = new Dictionary<int, string>();
            studentNumber.Add(1, "John");
            studentNumber.Add(2, "Mary");

            //Loop through dictionary

            foreach(KeyValuePair<int,string> keyValue in studentNumber)
            {
                Console.WriteLine("Key: {0}", keyValue.Key);
                Console.WriteLine("Value: {0}", keyValue.Value);
            }
        }

        public static void tryLists()
        {
            var names = new List<string> { "name", "John", "Mary"};

            //Add names to the list
            names.Add("Jack");
            names.Add("Megan");
            names.Remove("John");

            foreach(var i in names)
            {
                Console.WriteLine(i);
            }
        }

    }
}
